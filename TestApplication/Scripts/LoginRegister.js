﻿
$('#btn-login').click(function () {
    let username = $('#input-loginuser').val();
    let password = $('#input-loginpass').val();

    if (username != '' && password != '') {
        $.ajax({
            url: '/Home/Login',
            method: 'get',
            data: { username: username, password: password },
            success: function (result) {
                if (result.res == 2) {
                    window.location.href = '/Home/Welcome';
                    $('#input-loginuser,#input-loginuser').val('');
                }
                else {
                    $('#p-loginalert').css('visibility', 'visible');
                    $('#p-loginalert').text('Please check Username and Password');
                }

            }
        })
    }
    else {
        alert('Username and Password cannot be Empty');
    }
})

$('#btn-register').click(function () {
    let username = $('#input-registeruser').val();
    let password = $('#input-registerpass').val();

    if (username != '' && password != '') {
        $.ajax({
            url: '/Home/Register',
            method: 'post',
            data: { username: username, password: password },
            success: function (result) {
                $('#p-registeralert').css('visibility','visible');
                if (result.res == 1) {
                    $('#p-registeralert').text('Registration successfully');
                    $('#input-registeruser,#input-registerpass').val('');
                }
                else if (result.res == 2) {
                    $('#p-registeralert').text('Username Already Exists');
                    $('#input-registeruser,#input-registerpass').val('');
                }
                else {
                    alert('something went wrong')
                }

            }
        })
    }
    else {
        alert('Username and Password cannot be Empty');
    }
})

